window.slidesConfig = window.slidesConfig || {
  // Slide settings
  settings : {
    useBuilds: true,
    useGDDBranding: true,
    showCWS: true,
    showIAP: false

  },
  info: {
    // Personal info
    name: 'Ghislain Fourny & David Graf',
    pic: '',
    gplus: '',
    twitter: '',
    blog: '',
    slides: '',
    feedback: '',
    djbpDemo: '',
    djbpSrc: '',
    ioreaderDemo: '',
    ioreaderSrc: '',

    // GDD info
    location: 'Zurich, Switzerland',
    country: '',
    date: new Date('May 14 2012'),
    logo: ''
  }
};
